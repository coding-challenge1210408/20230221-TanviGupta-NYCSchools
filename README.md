20230221-TanviGupta-NYCSchools
=============================================
JP Morgan Coding Challenge: NYC Schools
=============================================

Demo
-------------
![NYC Schools - Animated gif demo](NYC_Schools_Android_Demo.gif)

Requirements
-------------
Create a native app (no react-native and no flutter) to provide information on NYC High Schools
1. Display a list of NYC High Schools
2. Selecting a school should show additional information about the school
  

### Features

This Android application can be used by school and students to browse NYC High schools, 
view each school's details and take actions like check address, call, email, etc

#### Presentation layer

The presentation layer consists of the following components:
* A splash launcher activity used to enhance the look of app and tie it to the theme/topic. Can also be used to pre-load any time-consuming data in background, before the actual application launch.
* A main activity that which shows the fetched list of schools for browsing.
* A school details activity which is used to display details of school selected from main activity.

### Features
* Supports Android devices with version between 21 and 33
* Supports both phone and tablet screen sizes
* Adds grid recycler view with 2 cells on phone size and 3 cells on tablet size devices
* Supports a custom animation for recycler view
* Address generator using Geocode and coordinates added as fallback for when there is no primary address available
* Uses string placeholders to add labels to text
* Used Spannables to apply different font to only the label part of the text
* Custom toolbar added with search and refresh icons
* Search functionality with results loaded with an animation effect
* Refresh functionality with results loaded with animation effect to give user feedback that refresh was completed
* Custom Splash launcher activity and custom app icons added
  * Added gradient colors effect based on NYC school official logo 
* Details activity loads with Explode animation
* Details screen's Tool bar customized to show school name in toolbar title
* Details view made scrollable to support small screen devices and the landscape orientation
* Icons added in details view which supports
  * Map: Clicking on the Map icon will open address of the school in map app.
  * Email: Clicking on the Email icon will open device's default email app with Send:to prefilled with school's contact email. If there are multiple browsers to choose from, a prompt will be shown to choose from.
  * Call: Clicking on Call icon will open phone dial pad with the school's contact number prefilled
  * Info: Clicking on Info icon will open the school's website in device's default browser. If there are multiple browsers to choose from, a prompt will be shown to choose from.
* Supports Accessibility
* Added both Android JUnit tests and Model tests

#### Data layer
The app uses a Model-View-ViewModel (MVVM) architecture for the presentation layer. The View and ViewModel communicate using LiveData and the following design principles:

* ViewModel objects don't have references to activities, fragments, or Android views. That would cause leaks on configuration changes, such as a screen rotation, because the system retains a ViewModel across the entire lifecycle of the corresponding view.
* ViewModel objects expose data using `LiveData` objects. `LiveData` allows you to observe changes to data across multiple components of your app without creating explicit and rigid dependency paths between them.
* Views subscribe to corresponding `LiveData` objects. Because `LiveData` is lifecycle-aware, it doesn’t push changes to the underlying data if the observer is not in an active state, and this helps to avoid many common bugs.
* Uses MVVM architecture to load data over network and store in database
* Room Database used for storing school data

#### Instructions to use the app
* The app supports all the versions between 21 and 33 and should be good to run on any Android device
  * App also supports tablet and can be run on it
* In order to test the app, clone or download the project and open with Android Studio
* Trigger "Sync with Gradle" in case it doesn't auto starts
* App should be good to install on Android emulator or attached physical Android device
## Note:
* Best viewed on actual physical android device due to the numerous animations implemented.

* This app could have been made better by using lazy loading technique to load data but since the available jsons do not support pagination, this was not implemented in this app.
  In this approach, data would be requested as per the request from view based on visibility. Optimisations could be made to preload the next set of data when user scrolls
  and is about to reach end of the available data instead of loading when he reaches the end.

#### Kotlin updates
* Re-written both ViewModels in Kotlin
* Added both ViewModel tests in Kotlin
* Re-written MainActivity in Kotlin
* Re-written SchoolDetailsActivity in Kotlin
* Re-written SplashLauncherActivity in Kotlin
* Re-written both Dao tests in Kotlin
* Re-written helper test class LiveDataTestUtil in Kotlin

#### New updates
* Added Dependency Injection
* Added Coroutines instead of ExecutorService for Database queries
* Re-wrote Dao, Repository and Database classes in Kotlin
* Added Dependency Injection in Android JUnit test cases
* Modified ViewModel unit test classes to use Mockito
* Modified Dao unit test classes to use Coroutines

