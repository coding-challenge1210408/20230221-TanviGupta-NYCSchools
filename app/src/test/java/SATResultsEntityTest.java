import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.android.nycschools.database.entity.SATResultsEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Local unit test, which will execute on the development machine (host).
 * Used to test SATResultsEntity
 */
public class SATResultsEntityTest {
    private String satResultsEntityJson;
    private String satResultsEntityEmptyJson;
    private Gson gson;

    @Before
    public void before() throws Exception {
        ClassLoader classLoader = this.getClass().getClassLoader();
        if (classLoader != null) {
            InputStream in = classLoader.getResourceAsStream("SATResultsEntity/SATResultEntity.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append('\n');
            }
            satResultsEntityJson = sb.toString();

            in = classLoader.getResourceAsStream("SATResultsEntity/SATResultEntityBadJson.json");
            bufferedReader = new BufferedReader(new InputStreamReader(in));
            sb = new StringBuilder();
            String line1;
            while ((line1 = bufferedReader.readLine()) != null) {
                sb.append(line1).append('\n');
            }
            satResultsEntityEmptyJson = sb.toString();
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    @Test
    public void testInitializeMediaFromJSON() throws Exception {
        SATResultsEntity satResultsEntity = gson.fromJson(satResultsEntityJson, SATResultsEntity.class);
        JSONObject satResultsEntityJsonObject = new JSONObject(satResultsEntityJson);

        assertEquals(satResultsEntity.getDbn(), satResultsEntityJsonObject.getString("dbn"));
        assertEquals(satResultsEntity.getSchool_name(), satResultsEntityJsonObject.getString("school_name"));
        assertEquals(satResultsEntity.getNum_of_sat_test_takers(), satResultsEntityJsonObject.getString("num_of_sat_test_takers"));
        assertEquals(satResultsEntity.getSat_critical_reading_avg_score(), satResultsEntityJsonObject.getString("sat_critical_reading_avg_score"));
        assertEquals(satResultsEntity.getSat_math_avg_score(), satResultsEntityJsonObject.getString("sat_math_avg_score"));
        assertEquals(satResultsEntity.getSat_writing_avg_score(), satResultsEntityJsonObject.getString("sat_writing_avg_score"));
    }

    @Test
    public void testBadJsonEmptyPropertiesN() {
        SATResultsEntity satResultsEntityEmpty = gson.fromJson(satResultsEntityEmptyJson, SATResultsEntity.class);

        assertNotNull(satResultsEntityEmpty.getDbn());
        assertNotNull(satResultsEntityEmpty.getSchool_name());
        assertNotNull(satResultsEntityEmpty.getNum_of_sat_test_takers());
        assertNotNull(satResultsEntityEmpty.getSat_critical_reading_avg_score());
        assertNotNull(satResultsEntityEmpty.getSat_math_avg_score());
        assertNotNull(satResultsEntityEmpty.getSat_writing_avg_score());
    }
}
