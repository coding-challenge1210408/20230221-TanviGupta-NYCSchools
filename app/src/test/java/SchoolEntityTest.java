import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.android.nycschools.database.entity.SchoolEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Local unit test, which will execute on the development machine (host).
 * Used to test SchoolEntity
 */
public class SchoolEntityTest {
    private String schoolEntityJson;
    private String schoolEntityEmptyJson;
    private Gson gson;

    @Before
    public void before() throws Exception {
        ClassLoader classLoader = this.getClass().getClassLoader();
        if (classLoader != null) {
            InputStream in = classLoader.getResourceAsStream("SchoolEntity/SchoolEntity.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append('\n');
            }
            schoolEntityJson = sb.toString();

            in = classLoader.getResourceAsStream("SchoolEntity/SchoolEntityBadJson.json");
            bufferedReader = new BufferedReader(new InputStreamReader(in));
            sb = new StringBuilder();
            String line1;
            while ((line1 = bufferedReader.readLine()) != null) {
                sb.append(line1).append('\n');
            }
            schoolEntityEmptyJson = sb.toString();
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    @Test
    public void testInitializeMediaFromJSON() throws Exception {
        SchoolEntity schoolEntity = gson.fromJson(schoolEntityJson, SchoolEntity.class);
        JSONObject schoolEntityJsonObject = new JSONObject(schoolEntityJson);

        assertEquals(schoolEntity.getDbn(), schoolEntityJsonObject.getString("dbn"));
        assertEquals(schoolEntity.getSchool_name(), schoolEntityJsonObject.getString("school_name"));
        assertEquals(schoolEntity.getOverview_paragraph(), schoolEntityJsonObject.getString("overview_paragraph"));
        assertEquals(schoolEntity.getPhone_number(), schoolEntityJsonObject.getString("phone_number"));
        assertEquals(schoolEntity.getSchool_email(), schoolEntityJsonObject.getString("school_email"));
        assertEquals(schoolEntity.getWebsite(), schoolEntityJsonObject.getString("website"));
        assertEquals(schoolEntity.getTotal_students(), schoolEntityJsonObject.getString("total_students"));
        assertEquals(schoolEntity.getPrimary_address_line_1(), schoolEntityJsonObject.getString("primary_address_line_1"));
        assertEquals(schoolEntity.getCity(), schoolEntityJsonObject.getString("city"));
        assertEquals(schoolEntity.getZip(), schoolEntityJsonObject.getString("zip"));
        assertEquals(schoolEntity.getLatitude(), schoolEntityJsonObject.getString("latitude"));
        assertEquals(schoolEntity.getLongitude(), schoolEntityJsonObject.getString("longitude"));

    }

    @Test
    public void testBadJsonEmptyPropertiesN() {
        SchoolEntity schoolEntityEmpty = gson.fromJson(schoolEntityEmptyJson, SchoolEntity.class);

        assertNotNull(schoolEntityEmpty.getDbn());
        assertNotNull(schoolEntityEmpty.getSchool_name());
        assertNotNull(schoolEntityEmpty.getOverview_paragraph());
        assertNotNull(schoolEntityEmpty.getPhone_number());
        assertNotNull(schoolEntityEmpty.getSchool_email());
        assertNotNull(schoolEntityEmpty.getWebsite());
        assertNotNull(schoolEntityEmpty.getTotal_students());
        assertNotNull(schoolEntityEmpty.getPrimary_address_line_1());
        assertNotNull(schoolEntityEmpty.getCity());
        assertNotNull(schoolEntityEmpty.getZip());
        assertNotNull(schoolEntityEmpty.getLatitude());
        assertNotNull(schoolEntityEmpty.getLongitude());
    }
}
