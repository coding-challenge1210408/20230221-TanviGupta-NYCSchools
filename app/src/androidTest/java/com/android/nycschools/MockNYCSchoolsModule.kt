package com.android.nycschools

import android.app.Application
import com.android.nycschools.database.dao.SATResultsDao
import com.android.nycschools.database.dao.SchoolDao
import com.android.nycschools.repository.SchoolRepository
import dagger.Module
import dagger.Provides
import org.mockito.Mockito
import javax.inject.Singleton

@Module
class MockNYCSchoolsModule {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return Mockito.mock(Application::class.java)
    }

    @Provides
    @Singleton
    fun provideSchoolRepository(): SchoolRepository {
        return Mockito.mock(SchoolRepository::class.java)
    }

    @Provides
    @Singleton
    fun provideSchoolDao(): SchoolDao {
        return Mockito.mock(SchoolDao::class.java)
    }

    @Provides
    @Singleton
    fun provideSATResultsDao(): SATResultsDao {
        return Mockito.mock(SATResultsDao::class.java)
    }
}