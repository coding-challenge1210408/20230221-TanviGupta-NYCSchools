package com.android.nycschools

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

/* Helps to run MockApplication */
@Suppress("unused")
class MockTestRunner : AndroidJUnitRunner() {
    @Throws(
        InstantiationException::class,
        IllegalAccessException::class,
        ClassNotFoundException::class
    )
    override fun newApplication(
        classLoader: ClassLoader,
        className: String,
        context: Context
    ): Application {
        return super.newApplication(
            classLoader,
            MockNYCSchoolsApplication::class.java.name,
            context
        )
    }
}