package com.android.nycschools.dao.test

import LiveDataTestUtil.blockingObserve
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room.inMemoryDatabaseBuilder
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.android.nycschools.database.SchoolRoomDatabase
import com.android.nycschools.database.dao.SchoolDao
import com.android.nycschools.database.entity.SchoolEntity
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.runner.RunWith

/**
 * Runs on actual device
 * Tests the implementation of [SchoolDao]
 */
@RunWith(AndroidJUnit4::class)
class SchoolDaoTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var schoolDao: SchoolDao
    private lateinit var db: SchoolRoomDatabase
    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = inMemoryDatabaseBuilder(context, SchoolRoomDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        schoolDao = db.schoolDao()
    }

    @After
    fun tearDown() {
        db.close()
    }

    /**
     * Testing insertion and deletion of Schools
     */
    @Test
    @Throws(Exception::class)
    fun insertDeleteGetAll() = runBlocking {
        val school1 = SchoolEntity("id1_school")
        val school2 = SchoolEntity("id2_school")
        val schools: MutableList<SchoolEntity> = ArrayList()
        schools.add(school1)
        schools.add(school2)
        schoolDao.insertAll(schools)
        var allSchools = schoolDao.getSchools().blockingObserve()
        TestCase.assertEquals(allSchools?.get(0)!!.school_name, school1.school_name)
        TestCase.assertEquals(allSchools[1].school_name, school2.school_name)
        schoolDao.deleteAll()
        allSchools = schoolDao.getSchools().blockingObserve()
        Assert.assertTrue(allSchools!!.isEmpty())
    }
}