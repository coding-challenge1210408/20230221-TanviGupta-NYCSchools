package com.android.nycschools.dao.test

import LiveDataTestUtil.blockingObserve
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room.inMemoryDatabaseBuilder
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.android.nycschools.database.SchoolRoomDatabase
import com.android.nycschools.database.dao.SATResultsDao
import com.android.nycschools.database.entity.SATResultsEntity
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.runner.RunWith

/**
 * Runs on actual device
 * Tests the implementation of [SATResultsDao]
 */
@RunWith(AndroidJUnit4::class)
class SATResultsDaoTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private lateinit var satResultsDao: SATResultsDao
    private lateinit var db: SchoolRoomDatabase
    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        db = inMemoryDatabaseBuilder(context, SchoolRoomDatabase::class.java)
                .allowMainThreadQueries()
                .build()
        satResultsDao = db.satResultsDao()
    }

    @After
    fun tearDown() {
        db.close()
    }

    /**
     * Testing insertion and deletion of SAT Results
     */
    @Test
    @Throws(Exception::class)
    fun insertDeleteGetAll() = runBlocking {
            val result1 = SATResultsEntity("id1_school")
            val result2 = SATResultsEntity("i2d_school")
            val resultsEntityList: MutableList<SATResultsEntity> = ArrayList()
            resultsEntityList.add(result1)
            resultsEntityList.add(result2)
            satResultsDao.insertAll(resultsEntityList)

            val testResults = satResultsDao.getSATResult(result1.dbn).blockingObserve()
            assertEquals(testResults!!.dbn, result1.dbn)
            val allResults = satResultsDao.getAllResults().blockingObserve()
            assertEquals(allResults?.get(0)!!.dbn, result1.dbn)
            assertEquals(allResults[1].dbn, result2.dbn)
            satResultsDao.deleteAll()
            val allSchools = satResultsDao.getAllResults().blockingObserve()
            assertTrue(allSchools!!.isEmpty())
        }
}