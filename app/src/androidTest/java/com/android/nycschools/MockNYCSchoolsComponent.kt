package com.android.nycschools

import com.android.nycschools.di.components.NYCSchoolsComponent
import com.android.nycschools.viewmodel.test.SATResultsViewModelTest
import com.android.nycschools.viewmodel.test.SchoolViewModelTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [MockNYCSchoolsModule::class])
interface MockNYCSchoolsComponent : NYCSchoolsComponent {
    fun inject(schoolViewModelTest: SchoolViewModelTest)
    fun inject(satResultsViewModelTest: SATResultsViewModelTest)
}