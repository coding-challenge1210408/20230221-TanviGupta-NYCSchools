package com.android.nycschools.viewmodel.test

import LiveDataTestUtil.blockingObserve
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.test.platform.app.InstrumentationRegistry
import com.android.nycschools.MockNYCSchoolsComponent
import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.database.entity.SchoolEntity
import com.android.nycschools.repository.SchoolRepository
import com.android.nycschools.viewmodel.SchoolViewModel
import junit.framework.TestCase
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import javax.inject.Inject
import org.mockito.Mockito.`when`


@RunWith(MockitoJUnitRunner::class)
class SchoolViewModelTest {
    // We need this to test our LiveData observers
    @get:Rule
    val rule = InstantTaskExecutorRule()

    // Inject mock of Repository
    @Inject
    lateinit var schoolRepository: SchoolRepository

    private lateinit var schoolViewModel: SchoolViewModel
    private lateinit var nYCSchoolsApplication: NYCSchoolsApplication

    @Before
    fun setUp() {
        nYCSchoolsApplication = InstrumentationRegistry.getInstrumentation()
            .targetContext.applicationContext as NYCSchoolsApplication
        nYCSchoolsApplication.setComponent()
        val mockNYCSchoolsComponent =
            nYCSchoolsApplication.nYCSchoolsComponent as MockNYCSchoolsComponent
        mockNYCSchoolsComponent.inject(this)
    }

    /**
     * Testing fetching of schools via School's ViewModel with mock data
     */
    @Test
    fun testGetSchools() {
        // Prepare mock data
        val school1 = SchoolEntity("id1_school")
        val school2 = SchoolEntity("id2_school")
        val schools: MutableList<SchoolEntity> = ArrayList()
        schools.add(school1)
        schools.add(school2)
        val liveDataToReturn = MutableLiveData<List<SchoolEntity>>()
            .apply { value = schools }
        `when`(schoolRepository.allSchools).thenReturn(liveDataToReturn)

        schoolViewModel = SchoolViewModel(nYCSchoolsApplication)
        val allSchools = schoolViewModel.allSchools.blockingObserve()

        TestCase.assertEquals(allSchools?.get(0)!!.school_name, school1.school_name)
        TestCase.assertEquals(allSchools[1].school_name, school2.school_name)
    }

    /**
     * Testing fetching of Schools when there is empty mock data
     */
    @Test
    fun testGetEmptySchool() {
        // Prepare empty mock data
        val emptyLiveDataToReturn = MutableLiveData<List<SchoolEntity>>()
        `when`(schoolRepository.allSchools).thenReturn(emptyLiveDataToReturn)

        schoolViewModel = SchoolViewModel(nYCSchoolsApplication)
        val allSchools = schoolViewModel.allSchools.blockingObserve()
        TestCase.assertNull(allSchools)
    }
}