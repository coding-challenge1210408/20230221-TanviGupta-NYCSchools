package com.android.nycschools.viewmodel.test

import LiveDataTestUtil.blockingObserve
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.test.platform.app.InstrumentationRegistry
import com.android.nycschools.MockNYCSchoolsComponent
import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.database.entity.SATResultsEntity
import com.android.nycschools.repository.SchoolRepository
import com.android.nycschools.viewmodel.SATResultsViewModel
import junit.framework.TestCase
import junit.framework.TestCase.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import javax.inject.Inject
import org.mockito.Mockito.`when`


@RunWith(MockitoJUnitRunner::class)
class SATResultsViewModelTest {
    // We need this to test our LiveData observers
    @get:Rule
    val rule = InstantTaskExecutorRule()

    // Inject mock of Repository
    @Inject
    lateinit var schoolRepository: SchoolRepository

    private lateinit var satResultsViewModel: SATResultsViewModel
    private lateinit var nYCSchoolsApplication: NYCSchoolsApplication

    @Before
    fun setUp() {
        nYCSchoolsApplication = InstrumentationRegistry.getInstrumentation()
            .targetContext.applicationContext as NYCSchoolsApplication
        nYCSchoolsApplication.setComponent()
        val mockNYCSchoolsComponent =
            nYCSchoolsApplication.nYCSchoolsComponent as MockNYCSchoolsComponent
        mockNYCSchoolsComponent.inject(this)
    }

    /**
    * Testing fetching of mock SAT Result via SATResults' ViewModel
    */
    @Test
    fun testGetSATResultsForSchool() {
        // Prepare mock data
        val testSATResultsEntity = SATResultsEntity("id_school")
        val liveDataToReturn = MutableLiveData<SATResultsEntity>()
            .apply { value = testSATResultsEntity }
        `when`(schoolRepository.getSATResultsForSchool(testSATResultsEntity.dbn)).thenReturn(liveDataToReturn)

        satResultsViewModel = SATResultsViewModel(nYCSchoolsApplication)
        val results = satResultsViewModel.getResultsForSchool(testSATResultsEntity.dbn).blockingObserve()
        TestCase.assertEquals(results!!.dbn, testSATResultsEntity.dbn)
    }

    /**
     * Testing fetching of SAT Results when there is empty mock data
     */
    @Test
    fun testGetEmptySATResultsForSchool() {

        // Prepare empty mock data
        val emptyLiveDataToReturn = MutableLiveData<SATResultsEntity>()

        `when`(schoolRepository.getSATResultsForSchool("test")).thenReturn(emptyLiveDataToReturn)
        satResultsViewModel = SATResultsViewModel(nYCSchoolsApplication)
        val results = satResultsViewModel.getResultsForSchool("test").blockingObserve()
        assertNull(results)
    }
}