package com.android.nycschools

import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.di.components.NYCSchoolsComponent

class MockNYCSchoolsApplication : NYCSchoolsApplication() {
    private lateinit var nycSchoolsComponent: NYCSchoolsComponent
    override val nYCSchoolsComponent: NYCSchoolsComponent
        get() = nycSchoolsComponent

    override fun setComponent() {
        nycSchoolsComponent = DaggerMockNYCSchoolsComponent.builder().mockNYCSchoolsModule(MockNYCSchoolsModule()).build()
        nycSchoolsComponent.inject(this)
    }
}