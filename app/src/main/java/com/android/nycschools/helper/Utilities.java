package com.android.nycschools.helper;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.android.nycschools.database.entity.SchoolEntity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/* Utilities helper class which doesn't hold any instances and provides helper methods */
public class Utilities {

    public static final String COMMA = ", ";

    public static String getFormattedAddress(SchoolEntity schoolEntity, Context context) {
        String address;

        // If primary address line is empty, use helper function to generate address based on co-ordinates
        if (schoolEntity.getPrimary_address_line_1().isEmpty()) {
            address = generateAddressFromCoOrdinates(context, Double.parseDouble(schoolEntity.getLatitude()), Double.parseDouble(schoolEntity.getLongitude()));

            // If generator fails, fallback to what we have
            if (address.isEmpty()) {
                address = getAddress(schoolEntity);
            }
        } else {
           address = getAddress(schoolEntity);
        }
        return address;
    }

    private static String getAddress(SchoolEntity schoolEntity) {
        return schoolEntity.getPrimary_address_line_1() + COMMA +
                schoolEntity.getCity() + COMMA +
                schoolEntity.getZip();
    }

    private static String generateAddressFromCoOrdinates(Context context, double latitude, double longitude) {
        List<Address> addressList;
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String calculatedAddress = "";
        try {
            addressList = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addressList != null && addressList.size() > 0) {
                calculatedAddress = addressList.get(0).getAddressLine(0);
            }

        } catch (IOException e) {
            return calculatedAddress;
        }

        return calculatedAddress;
    }

}
