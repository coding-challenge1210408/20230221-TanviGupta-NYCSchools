package com.android.nycschools.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.android.nycschools.database.entity.SchoolEntity

/**
 * Data access object for School
 */
@Dao
interface SchoolDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(schoolsList: List<SchoolEntity>)

    @Query("DELETE FROM school_table")
    suspend fun deleteAll()

    /* Using LiveData here to observe any changes to data */
    @Transaction
    @Query("SELECT * FROM school_table ORDER BY school_name ASC")
    fun getSchools(): LiveData<List<SchoolEntity>>

    @Transaction
    @Query("SELECT * FROM school_table where school_name like :searchString ORDER BY school_name ASC")
    fun getSchoolsFiltered(searchString: String): LiveData<List<SchoolEntity>>
}