package com.android.nycschools.database.dao

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.room.*
import com.android.nycschools.database.entity.SATResultsEntity

/**
 * Data access object for SAT Results
 */
@Dao
interface SATResultsDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(satResultsList: List<SATResultsEntity>)

    @Query("DELETE FROM sat_results_table")
    suspend fun deleteAll()

    /* Using LiveData here to observe any changes to data */
    @Transaction
    @Query("SELECT * FROM sat_results_table where dbn = :schoolDBN")
    fun getSATResult(schoolDBN: String): LiveData<SATResultsEntity>

    @Transaction
    @Query("SELECT * FROM sat_results_table  ORDER BY school_name ASC")
    @VisibleForTesting
    fun getAllResults(): LiveData<List<SATResultsEntity>>
}