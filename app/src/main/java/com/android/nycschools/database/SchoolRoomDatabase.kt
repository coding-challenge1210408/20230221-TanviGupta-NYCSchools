package com.android.nycschools.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.nycschools.database.dao.SATResultsDao
import com.android.nycschools.database.dao.SchoolDao
import com.android.nycschools.database.entity.SATResultsEntity
import com.android.nycschools.database.entity.SchoolEntity

/**
 * The database class for Room
 */
@Database(
    entities = [SchoolEntity::class, SATResultsEntity::class],
    version = 1,
    exportSchema = false
)
abstract class SchoolRoomDatabase : RoomDatabase() {
    abstract fun schoolDao(): SchoolDao
    abstract fun satResultsDao(): SATResultsDao
}