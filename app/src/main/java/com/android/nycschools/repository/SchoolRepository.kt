package com.android.nycschools.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.database.dao.SATResultsDao
import com.android.nycschools.database.dao.SchoolDao
import com.android.nycschools.database.entity.SATResultsEntity
import com.android.nycschools.database.entity.SchoolEntity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.*
import java.io.IOException
import java.util.*
import javax.inject.Inject


/**
 * Repository class that manages queries and fetches data over network
 */
class SchoolRepository(context: Context) {
    @Inject
    lateinit var mSchoolDao: SchoolDao

    @Inject
    lateinit var mResultsDao: SATResultsDao

    /**
     * Will fetch Schools list as LiveData so that it can be executed in the background
     * @return results list of all schools
     */
    val allSchools: LiveData<List<SchoolEntity>>

    init {
        (context as NYCSchoolsApplication).nYCSchoolsComponent.inject(this)
        allSchools = mSchoolDao.getSchools()
    }

    /**
     * Searches in DB
     * @param searchString String to search
     * @return results read from database
     */
    fun getFilteredSchools(searchString: String): LiveData<List<SchoolEntity>> {
        return mSchoolDao.getSchoolsFiltered(searchString)
    }

    /**
     * Get SAT Results for School with given DBN
     * @param schoolDBN DBN of the school
     * @return school found with the given DBN
     */
    fun getSATResultsForSchool(schoolDBN: String): LiveData<SATResultsEntity> {
        return mResultsDao.getSATResult(schoolDBN)
    }

    /**
     * Insert Schools into DB in background
     * @param schools List of schools to be inserted
     */
    private fun insertAll(schools: List<SchoolEntity>) {
        CoroutineScope(Dispatchers.IO).launch { mSchoolDao.insertAll(schools) }
    }

    /**
     * Insert SAT Results into DB in background
     * @param results List of SAT results to be inserted
     */
    private fun insertAllSATResults(results: List<SATResultsEntity>) {
        CoroutineScope(Dispatchers.IO).launch { mResultsDao.insertAll(results) }
    }

    /**
     * From here lies all code related to REST API calls using OKHTTP.
     * We can put them in another class to handle them.
     */
    fun loadSchools() {
        fetchSchoolsData()
        fetchSATResults()
    }

    /**
     * Fetches School Data from NYC Schools API
     */
    private fun fetchSchoolsData() {
        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url(NYC_SCHOOLS_URL)
            .method("GET", null)
            .build()
        try {
            client.newCall(request).enqueue(object : Callback {
                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    val jsonData = response.body?.string()
                    // Load data as School object using Gson
                    // Note: Ideal way here would be to lazy load the data but since the available json doesn't support pagination, we are loading all data at once
                    val listType = object : TypeToken<List<SchoolEntity?>?>() {}.type
                    val schools = Gson().fromJson<List<SchoolEntity>>(jsonData, listType)
                    insertAll(schools)
                }

                override fun onFailure(call: Call, e: IOException) {
                    call.cancel()
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Fetches SAT Results Data from NYC Schools API
     */
    private fun fetchSATResults() {
        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url(NYC_SCHOOLS_SAT_RESULTS_URL)
            .method("GET", null)
            .build()
        try {
            client.newCall(request).enqueue(object : Callback {
                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    val jsonData = response.body?.string()
                    // Load data as SAT Results object using Gson
                    // Note: Ideal way here would be to lazy load the data but since the available json doesn't support pagination, we are loading all data at once
                    val listType = object : TypeToken<List<SATResultsEntity?>?>() {}.type
                    val satResults = Gson().fromJson<List<SATResultsEntity>>(jsonData, listType)
                    insertAllSATResults(satResults)
                }

                override fun onFailure(call: Call, e: IOException) {
                    call.cancel()
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private const val NYC_SCHOOLS_URL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        private const val NYC_SCHOOLS_SAT_RESULTS_URL =
            "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
}