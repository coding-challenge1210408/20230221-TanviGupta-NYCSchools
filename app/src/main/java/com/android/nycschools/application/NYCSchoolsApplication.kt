package com.android.nycschools.application

import android.app.Application
import com.android.nycschools.di.components.DaggerNYCSchoolsComponent
import com.android.nycschools.di.components.NYCSchoolsComponent
import com.android.nycschools.di.modules.AppModule
import com.android.nycschools.di.modules.NYCSchoolsModule

open class NYCSchoolsApplication : Application() {
    private lateinit var nycSchoolsComponent: NYCSchoolsComponent
    override fun onCreate() {
        super.onCreate()
        setComponent()
    }

    open val nYCSchoolsComponent: NYCSchoolsComponent
        get() = nycSchoolsComponent

    open fun setComponent() {
        nycSchoolsComponent = DaggerNYCSchoolsComponent.builder()
            .appModule(AppModule(this))
            .nYCSchoolsModule(NYCSchoolsModule(this))
            .build()
        nycSchoolsComponent.inject(this)
    }
}