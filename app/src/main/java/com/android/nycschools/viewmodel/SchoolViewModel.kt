package com.android.nycschools.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.repository.SchoolRepository
import com.android.nycschools.database.entity.SchoolEntity
import javax.inject.Inject

/**
 * ViewModel class for School
 */
class SchoolViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {
    @Inject
    lateinit var schoolRepository: SchoolRepository
    val allSchools: LiveData<List<SchoolEntity>>

    init {
        (application as NYCSchoolsApplication).nYCSchoolsComponent.inject(this)
        allSchools = schoolRepository.allSchools
    }

    fun getFilteredSchools(searchString: String): LiveData<List<SchoolEntity>> {
        return schoolRepository.getFilteredSchools(searchString)
    }

    fun loadSchools() {
        schoolRepository.loadSchools()
    }
}