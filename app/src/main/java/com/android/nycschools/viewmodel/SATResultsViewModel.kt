package com.android.nycschools.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.repository.SchoolRepository
import com.android.nycschools.database.entity.SATResultsEntity
import javax.inject.Inject

/**
 * ViewModel class for SAT Results
 */
class SATResultsViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {
    @Inject
    lateinit var mSchoolRepository: SchoolRepository

    init {
        (application as NYCSchoolsApplication).nYCSchoolsComponent.inject(this)
    }

    fun getResultsForSchool(schoolDBN: String): LiveData<SATResultsEntity> {
        return mSchoolRepository.getSATResultsForSchool(schoolDBN)
    }
}