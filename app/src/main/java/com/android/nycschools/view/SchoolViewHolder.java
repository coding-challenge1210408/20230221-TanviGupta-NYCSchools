package com.android.nycschools.view;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.nycschools.R;
import com.android.nycschools.database.entity.SchoolEntity;
import com.android.nycschools.helper.Utilities;

/**
 * ViewHolder class for Recycler view
 */
public class SchoolViewHolder extends RecyclerView.ViewHolder    {
    private final TextView schoolName;
    private final TextView schoolAddress;
    private final TextView schoolContactNumber;
    private final TextView schoolWebsite;
    private SchoolEntity schoolEntity;

    private SchoolViewHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), SchoolDetailsActivity.class);
            intent.putExtra("SchoolEntity", schoolEntity);

            // Open School Details activity with animation
            ActivityOptions options =
                    ActivityOptions.makeSceneTransitionAnimation((Activity) v.getContext());
            v.getContext().startActivity(intent,options.toBundle());
        });

        schoolName = itemView.findViewById(R.id.school_name);
        schoolAddress = itemView.findViewById(R.id.school_address);
        schoolContactNumber = itemView.findViewById(R.id.school_contact_number);
        schoolWebsite = itemView.findViewById(R.id.school_website);
    }

    protected void bind(SchoolEntity schoolEntity) {
        this.schoolEntity = schoolEntity;
        schoolName.setText(schoolEntity.getSchool_name());

        // Use Utility helper function to generate address with available data, adding fallbacks to generate address from coordinates
        String address = Utilities.getFormattedAddress(schoolEntity, schoolAddress.getContext());

        // Use placeholders to add labels to the following strings
        // Call helper function to apply Bold style to only the label part of the string
        schoolAddress.setText(applyFontOnlyToLabelOfString(schoolAddress.getContext().getResources().getString(R.string.address_label, address)));
        schoolContactNumber.setText(applyFontOnlyToLabelOfString(schoolContactNumber.getContext().getResources().getString(R.string.contact_number_label, schoolEntity.getPhone_number())));
        schoolWebsite.setText(applyFontOnlyToLabelOfString(schoolContactNumber.getContext().getResources().getString(R.string.website_label, schoolEntity.getWebsite())));
    }

    private SpannableStringBuilder applyFontOnlyToLabelOfString(String stringWithPlaceholder) {
        // Only take text before space, which will be just label
        String[] addressLabel = stringWithPlaceholder.split(" ", 2);

        SpannableString s1 = new SpannableString(addressLabel[0]);
        SpannableString s2 = new SpannableString(addressLabel[1]);

        // Apply bold style only to label part of string
        s1.setSpan(new StyleSpan(Typeface.BOLD), 0, s1.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(s1).append(" ");
        builder.append(s2);
        return builder;
    }

    protected static SchoolViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schools_recyclerview_item, parent, false);
        return new SchoolViewHolder(view);
    }
}
