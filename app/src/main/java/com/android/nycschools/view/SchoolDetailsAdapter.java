package com.android.nycschools.view;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.nycschools.R;
import com.android.nycschools.database.entity.SchoolEntity;

/**
 * Adapter class for Recycler View
 */
public class SchoolDetailsAdapter extends ListAdapter<SchoolEntity, SchoolViewHolder> {

    public SchoolDetailsAdapter(@NonNull DiffUtil.ItemCallback<SchoolEntity> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return SchoolViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(SchoolViewHolder holder, int position) {
        int margin = (int) holder.itemView.getContext().getResources().getDimension(R.dimen.recycler_view_margin);
        RecyclerView.LayoutParams itemLayoutParams = (GridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
        if (itemLayoutParams != null) { //Set margins based on no of columns which can be 2 or 3 depending on screen size
            switch (position % holder.itemView.getResources().getInteger(R.integer.no_of_columns)) {
                case 0:
                    itemLayoutParams.setMargins(margin / 2, margin / 2, 0, 0);
                    break;
                case 1:
                    itemLayoutParams.setMargins(margin / 2, margin / 2, margin / 2, 0);
                    break;
                case 2:
                    itemLayoutParams.setMargins(0, margin / 2, margin / 2, 0);
                    break;
            }
            holder.itemView.setLayoutParams(itemLayoutParams);
        }
        SchoolEntity current = getItem(position);
        holder.bind(current);
    }

    /**
     * Used to see the difference between two lists
     */
    public static class SchoolDiff extends DiffUtil.ItemCallback<SchoolEntity> {

        @Override
        public boolean areItemsTheSame(@NonNull SchoolEntity oldItem, @NonNull SchoolEntity newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull SchoolEntity oldItem, @NonNull SchoolEntity newItem) {
            return oldItem.getSchool_name().equals(newItem.getSchool_name());
        }
    }
}
