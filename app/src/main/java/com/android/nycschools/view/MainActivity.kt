package com.android.nycschools.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.R
import com.android.nycschools.database.entity.SchoolEntity
import com.android.nycschools.view.SchoolDetailsAdapter.SchoolDiff
import com.android.nycschools.view.customviews.CustomRecyclerView
import com.android.nycschools.viewmodel.SchoolViewModel
import java.util.*
import javax.inject.Inject

/**
 * Main activity which shows the fetched list of schools via customized recycler view
 */
class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {
    @Inject
    lateinit var mSchoolViewModel: SchoolViewModel
    private lateinit var mSchoolDetailsAdapter: SchoolDetailsAdapter
    private lateinit var customRecyclerView: CustomRecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (applicationContext as NYCSchoolsApplication).nYCSchoolsComponent.inject(this)

        setContentView(R.layout.activity_main)
        customRecyclerView = findViewById(R.id.recyclerview)
        val mRecyclerViewAnimationController = AnimationUtils.loadLayoutAnimation(this, R.anim.recycler_view_layout_animation)
        mSchoolDetailsAdapter = SchoolDetailsAdapter(SchoolDiff())
        customRecyclerView.adapter = mSchoolDetailsAdapter

        // Read number of columns for grid view from resources as we want to show 2 columns for phone size and 3 for tablet size
        customRecyclerView.layoutManager = GridLayoutManager(this, resources.getInteger(R.integer.no_of_columns))
        customRecyclerView.layoutAnimation = mRecyclerViewAnimationController

        // Update the cached copy of the schools in the adapter
        // Note: Ideal way here would be to lazy load the data into the view but since the available json doesn't support pagination, we are loading all data at once
        mSchoolViewModel.allSchools.observe(this) { schoolEntityList: List<SchoolEntity?> ->
            mSchoolDetailsAdapter.submitList(schoolEntityList)
            customRecyclerView.scheduleLayoutAnimation()
        }
        mSchoolViewModel.loadSchools()
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    /**
     * Callback when menu options are created
     * @param menu The options menu in which you place your items.
     */
    @SuppressLint("ResourceType")
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val search = menu.findItem(R.id.action_search)
        val searchView = search.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return true
    }

    /**
     * Callback when menu options are selected
     * @param item The menu item that was selected.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.menu_refresh) {
            // Add this to trigger animation after data loads so user sees the screen refreshing
            mSchoolViewModel.allSchools.observe(this) {
                customRecyclerView.smoothScrollToPosition(0)
                // Notify only a small range to increase efficiency
                mSchoolDetailsAdapter.notifyItemRangeChanged(0, 10)
                customRecyclerView.scheduleLayoutAnimation()
            }
            mSchoolViewModel.loadSchools()
            return true
        } else if (id == R.id.action_search) {
            val searchView = item.actionView as SearchView?
            searchView!!.setOnQueryTextListener(this)
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Called when the user submits the search query
     * @param query the query text that is to be submitted
     */
    override fun onQueryTextSubmit(query: String): Boolean {
        return false
    }

    /**
     * Called when the search query text is changed by the user
     * @param newText the new content of the query text field.
     */
    override fun onQueryTextChange(newText: String): Boolean {
        // Updates Search filter and runs DB Query using search string non capitalized.
        var newTextQuery = newText
        newTextQuery = "%" + newTextQuery.lowercase(Locale.getDefault()) + "%"
        mSchoolViewModel.getFilteredSchools(newTextQuery).observe(this) { schoolEntityList: List<SchoolEntity?> ->
            mSchoolDetailsAdapter.submitList(schoolEntityList)
            customRecyclerView.scheduleLayoutAnimation()
        }
        return true
    }
}