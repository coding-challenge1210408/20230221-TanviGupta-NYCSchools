package com.android.nycschools.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.android.nycschools.R

/**
 * Used to enhance the look of app and tie it to the theme/topic
 * Can also be used to pre-load any time-consuming data in background, before the actual application launch
 */
class SplashLauncherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Duration of wait
        val splashDisplayLength = 1000
        Handler(Looper.getMainLooper()).postDelayed({

            // Create an Intent that will start the MainActivity.
            startActivity(Intent(this@SplashLauncherActivity, MainActivity::class.java))
            // Use animation to fold the view in middle
            overridePendingTransition(R.anim.from_middle, R.anim.to_middle)
            finish()
        }, splashDisplayLength.toLong())
    }
}