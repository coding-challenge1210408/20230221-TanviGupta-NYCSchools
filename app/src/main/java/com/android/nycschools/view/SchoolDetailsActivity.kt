package com.android.nycschools.view

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.transition.Explode
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.android.nycschools.R
import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.database.entity.SATResultsEntity
import com.android.nycschools.database.entity.SchoolEntity
import com.android.nycschools.viewmodel.SATResultsViewModel
import javax.inject.Inject

/**
 * Details activity which shows the details of the school selected
 */
class SchoolDetailsActivity : AppCompatActivity() {
    @Inject
    lateinit var mSatResultsViewModel: SATResultsViewModel

    private lateinit var mSchoolEntity: SchoolEntity

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (applicationContext as NYCSchoolsApplication).nYCSchoolsComponent.inject(this)

        // Use this to set animation so activity transitions smoothly with a special effect, and this should be called before content view is set
        setAnimation()
        setContentView(R.layout.activity_school_details)
        val toolbar = findViewById<Toolbar>(R.id.school_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // School object to setup the page
        mSchoolEntity = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.getParcelableExtra("SchoolEntity", SchoolEntity::class.java)!!
        } else {
            @Suppress("DEPRECATION")
            intent.getParcelableExtra("SchoolEntity")!!
        }

        // Show school's name as title of the Tool bar
        supportActionBar!!.title = mSchoolEntity.school_name

        // Setup UI with School object from intent
        // (School name text is set as title of the tool bar)
        val descTextView = findViewById<TextView>(R.id.description_text)
        descTextView.text = mSchoolEntity.overview_paragraph
        descTextView.isSingleLine = false
        descTextView.setHorizontallyScrolling(false)
        setupDirectionsButton()
        setupEmailButton()
        setupCallButton()
        setupInfoButton()

        // Loads SAT Results for the School
        val result = mSatResultsViewModel.getResultsForSchool(mSchoolEntity.dbn)
        result.observe(this) { resultsEntity: SATResultsEntity? -> satResultsUpdated(resultsEntity) }
    }

    /**
     * Function to set up Explode animation
     */
    private fun setAnimation() {
        val explode = Explode()
        explode.duration = 500
        explode.interpolator = DecelerateInterpolator()
        window.exitTransition = explode
        window.enterTransition = explode
    }

    /**
     * Setup click listener for Directions icon, will open address of the school in map app
     */
    private fun setupDirectionsButton() {
        val directionsButton = findViewById<ImageButton>(R.id.directions_button)
        directionsButton.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:" + mSchoolEntity.latitude + "," + mSchoolEntity.longitude + "?q=" + mSchoolEntity.school_name)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }

    /**
     * Setup click listener for Email icon, will open device's default email app with Send:to prefilled with school's contact email.
     * If there are multiple browsers to choose from, a prompt will be shown to choose from.
     */
    private fun setupEmailButton() {
        val emailButton = findViewById<ImageButton>(R.id.email_button)
        emailButton.setOnClickListener { v: View ->
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:" + mSchoolEntity.school_email)
            v.context.startActivity(Intent.createChooser(intent, EMAIL_PROMPT))
        }
    }

    /**
     * Setup click listener for Call icon, will open phone dial pad with the school's contact number prefilled
     */
    private fun setupCallButton() {
        val callButton = findViewById<ImageButton>(R.id.call_button)
        callButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + mSchoolEntity.phone_number)
            startActivity(intent)
        }
    }

    /**
     * Setup click listener for Info icon, will open the school's website in device's default browser.
     * If there are multiple browsers to choose from, a prompt will be shown to choose from.
     */
    private fun setupInfoButton() {
        val infoButton = findViewById<ImageButton>(R.id.info_button)
        infoButton.setOnClickListener { v: View ->
            var website = mSchoolEntity.website

            // Some websites don't have scheme set, append it to enable website to open in device's default browser
            if (!website.startsWith(HTTP) && !website.startsWith(HTTPS)) {
                website = HTTPS + website
            }
            val websiteIntentUri = Uri.parse(website)
            val websiteIntent = Intent(Intent.ACTION_VIEW, websiteIntentUri)
            v.context.startActivity(Intent.createChooser(websiteIntent, BROWSE_PROMPT))
        }
    }

    /**
     * Updates the SAT results once available from DB
     * @param resultsEntity received from Live Data's observable
     */
    private fun satResultsUpdated(resultsEntity: SATResultsEntity?) {
        if (resultsEntity != null) {
            val satResultsTextView = findViewById<TextView>(R.id.sat_results_text_view)
            satResultsTextView.text = resultsEntity.toString()
        }
    }

    companion object {
        const val HTTP = "http://"
        const val HTTPS = "https://"
        const val EMAIL_PROMPT = "Email School"
        const val BROWSE_PROMPT = "Browse School's Website with"
    }
}