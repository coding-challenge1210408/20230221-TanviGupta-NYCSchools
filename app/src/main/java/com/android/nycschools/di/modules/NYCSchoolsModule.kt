package com.android.nycschools.di.modules

import android.app.Application
import androidx.room.Room.databaseBuilder
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.android.nycschools.repository.SchoolRepository
import com.android.nycschools.database.SchoolRoomDatabase
import com.android.nycschools.database.dao.SATResultsDao
import com.android.nycschools.database.dao.SchoolDao
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Singleton

@Module(includes = [AppModule::class])
class NYCSchoolsModule(mApplication: Application) {

    private lateinit var schoolRoomDatabase: SchoolRoomDatabase
    private var libraryApplication = mApplication

    private val databaseCallback = object : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            CoroutineScope(Dispatchers.IO).launch {
                val schoolDao = schoolRoomDatabase.schoolDao()
                schoolDao.deleteAll()
                val resultsDao = schoolRoomDatabase.satResultsDao()
                resultsDao.deleteAll()
            }
        }
    }

    @Singleton
    @Provides
    fun provideSchoolRepository(): SchoolRepository {
        return SchoolRepository(libraryApplication)
    }

    @Singleton
    @Provides
    fun provideSchoolRoomDatabase(): SchoolRoomDatabase {
        schoolRoomDatabase = databaseBuilder(
            libraryApplication,
            SchoolRoomDatabase::class.java, "school_database"
        ).addCallback(databaseCallback).build()
        return schoolRoomDatabase
    }

    @Singleton
    @Provides
    fun provideSchoolDao(schoolRoomDatabase: SchoolRoomDatabase): SchoolDao {
        return schoolRoomDatabase.schoolDao()
    }

    @Singleton
    @Provides
    fun provideSATResultsDao(schoolRoomDatabase: SchoolRoomDatabase): SATResultsDao {
        return schoolRoomDatabase.satResultsDao()
    }
}