package com.android.nycschools.di.components

import com.android.nycschools.application.NYCSchoolsApplication
import com.android.nycschools.repository.SchoolRepository
import com.android.nycschools.di.modules.AppModule
import com.android.nycschools.di.modules.NYCSchoolsModule
import com.android.nycschools.view.MainActivity
import com.android.nycschools.view.SchoolDetailsActivity
import com.android.nycschools.viewmodel.SATResultsViewModel
import com.android.nycschools.viewmodel.SchoolViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NYCSchoolsModule::class])
interface NYCSchoolsComponent {
    fun inject(nycSchoolsApplication: NYCSchoolsApplication)
    fun inject(mainActivity: MainActivity)
    fun inject(schoolDetailsActivity: SchoolDetailsActivity)
    fun inject(schoolViewModel: SchoolViewModel)
    fun inject(satResultsViewModel: SATResultsViewModel)
    fun inject(schoolRepository: SchoolRepository)
}